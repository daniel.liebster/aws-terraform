#!/usr/bin/env python3

import requests
import json

result = None
with open('data.json', 'r') as test_data:
    json_data = json.load(test_data)
with open('../assets/lb_dns_name.txt', 'r') as lb_dns_name:
    lb_addr = lb_dns_name.read().strip()

request = f"http://{lb_addr}/builds"
print(f"Testing:  {request}")
try:
    result = requests.post(request, json=json_data)
except:
    print("\nConnection error, please allow 3 minutes after instance creation for the LB's DNS to propagate")

if result:
    print(result)
    print(result.content)
