These are my results for a 4 hour interview code challenge to automate a single cmd deployment of a JSON data handler on AWS.


1. Please run the included 'deploy_service.sh' script to deploy the build reporting service in AWS us-west-2.

2. The test script test/test_lb.py will run a quick end-to-end validation against thoe load balancer ip.  A successful run returns a 200 code, for example:

$ cd test/
$ ./test_lb.py 
Testing:  http://POST-elb-344175353.us-west-2.elb.amazonaws.com/builds
<Response [200]>
b'{"latest": {"build_date": "1506747777", "ami_id": "ami-9f0ae4e5", "commit_hash": "d1541c88258ccb3ee565fa1d2322e04cdc5a7777"}}'
$ 

