import json
import cherrypy


def error_page_404(status, message, traceback, version):
    return "404 Error, Resource not found"


class HomeController():
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def builds(self, **kwargs):
        latest_build = {
            'runtime_seconds': "",
            'build_date': 0,
            'result': "",
            'output': ""
        }
        data = cherrypy.request.json
        for build in data['jobs']['Build base AMI']['Builds']:
            latest_build_date = int(latest_build['build_date'])
            build_date = int(build['build_date'])
            if build_date > latest_build_date:
                latest_build = build

        latest_output = latest_build['output'].split()
        latest_ami_id = latest_output[2]
        latest_build_hash = latest_output[3]
        latest_build_date = latest_build['build_date']

        latest_return = {'latest': {'build_date': latest_build_date,
                                    'ami_id': latest_ami_id,
                                    'commit_hash': latest_build_hash
                                    }
                         }

        return latest_return


def start_server():
    cherrypy.server.socket_host = '0.0.0.0'
    cherrypy.tree.mount(HomeController(), '/')
    cherrypy.config.update({'error_page.404': error_page_404})
    cherrypy.config.update({'server.socket_port': 8080})
    cherrypy.engine.start()


if __name__ == '__main__':
    start_server()
