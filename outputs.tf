output "key-name" {
  value = aws_key_pair.POST-handler-key.key_name
}

output "lb-dns-name" {
  value = aws_elb.POST-handler-elb.dns_name
}

output "instance-ip-public" {
  value = aws_instance.POST-handler.public_ip
}